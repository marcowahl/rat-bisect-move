# What #

A stumpwm module to control the rat with the keyboard.

Concretely:
- Choose a location where the rat shall be.
- Successively choose directions that get the rat nearer to the
  location.

The core idea is to move the rat by choosing one of four possible
directions and most of the time divide the area of reachable locations
in half.

# Why #

Sometimes you don't have a rat and you want to position the rat at a
certain location of the screen.  This can be done with the commands
ratwarp and ratrelwarp but might be annoying.

# Usage #

it's a bit messy.  somehow get to a state with background only.  then
apply the keys to move the rat.

TODO: clean up the mess!

The standard bindings are:
- s-{left,right,down,up} to move the rat in the respective direction.
- s-c to click.
- s-b to set the image of the current desktop as background and then
  hide all windows.  (This is essential to see a graphical
  representation, see below.)

A graphical representation of the current area is drawn onto the
_background_ of the screen.  (I did not find a way to draw something
temporarily on top of any window.)

Trick to see the graphical representation together with the windows:
Take a snapshot and put it as background.

Key binding s-b sets the current screen as background-image and
closes all windows.  So one can see the rat in the context of the
windows.  Bring back the windows with the typical stumpwm commands and
click (e.g. with s-c).

Okay this is just a start.  Contribution and communication are very
welcome.

# Dependencies #

- Stumpwm module screenshot.  See
  https://github.com/stumpwm/stumpwm-contrib/.
  - zpng lisp library.
- Imagemagick
- xsetbg
  - xloadimage

# Related #

There is program keynav which implements also the idea of bisection.
keynav is much more mature than rat-bisect-move.  See
http://www.semicomplete.com/blog/projects/keynav.
