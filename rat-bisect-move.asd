;;;; rat-bisect-move.asd

(asdf:defsystem #:rat-bisect-move
  :serial t
  :description "Bisection style movement of the rat"
  :author "Marco Wahl <marcowahlsoft@gmail.com>"
  :license "GPLv3"
  :depends-on (#:stumpwm #:screenshot)
  :components ((:file "package")
               (:file "rat-bisect-move")))
