;;;; rat-bisect-move.lisp --- Rat movement mostly by bisection


(in-package #:rat-bisect-move)


(export
 '(draw-rectangle-onto-background
   black-background
   screenshot-as-background
   take-screenshot-as-background
   blured-screenshot-as-background
   blured-screenshot-as-background-and-clear-frames
   rat-bisect-left
   rat-bisect-right
   rat-bisect-up
   rat-bisect-down))


;; Configuration

(defvar *max-rat-dist* 1024)


;; Graphical elements to (possibly) please the user

(defun draw-rectangle-onto-background (x y w h)
  "Draw a rectangle in a certain style."
  (xlib:draw-rectangle
   (screen-root (current-screen))
   (xlib:create-gcontext
    :drawable (screen-root (current-screen))
    :foreground (random (- (* 256 256 256) 1))
    :background (random (- (* 256 256 256) 1))
    :line-style :double-dash
    :line-width 3)
   x y w h))

(defun screenshot-as-background ()
  "Screenshot of current windows of all screens."
  (let ((screenshot-file-name "/tmp/stumpwm-screenshot.png"))
    (screenshot:screenshot screenshot-file-name)
    (run-shell-command
     (format nil "xsetbg ~A" screenshot-file-name))))

(defun put-screenshot-file-as-background ()
  (let ((screenshot-file-name "/tmp/stumpwm-screenshot.png"))
    (run-shell-command
     (format nil "xsetbg ~A" screenshot-file-name))))

(defcommand take-screenshot-as-background () ()
  "Set current screen as background."
  (screenshot-as-background)
  (mapc (lambda (x)
          (stumpwm::clear-frame x (stumpwm::current-group)))
        (stumpwm::group-frames (stumpwm::current-group))))

(defun blured-screenshot-as-background ()
  "Screenshot of current windows of all screens.
Possibly change the screenshot a bit to allow the user to recognize it
as screenshot."
  (let ((screenshot-file-name "/tmp/stumpwm-screenshot.png")
        (screenshot-modified-file-name "/tmp/stumpwm-modif-screenshot.png"))
    (screenshot:screenshot screenshot-file-name)
    (run-shell-command
     (format nil "convert ~A -channel RGBA -blur 0x1 ~A ; xsetbg ~A"
             screenshot-file-name
             screenshot-modified-file-name
             screenshot-modified-file-name))))

(defcommand blured-screenshot-as-background-and-clear-frames () ()
  "Blur current screen, set as background and hide all windows."
  (blured-screenshot-as-background)
  (mapc (lambda (x)
          (stumpwm::clear-frame x (stumpwm::current-group)))
        (stumpwm::group-frames (stumpwm::current-group))))

(defcommand black-background () ()
  "Set background to black."
  (let ((screenshot-file-name "/tmp/black.png"))
    (run-shell-command
     (format nil "convert -size 1x1 xc:black ~A ; xsetbg ~A"
             screenshot-file-name
             screenshot-file-name))))


;; Moves for the rat and a click

(defun draw-rectangle-onto-current-as-bg (x y w h)
           ;; (screenshot-as-background)
           ;; (fclear)
           (draw-rectangle-onto-background
            x y w h))

(let ((hspan *max-rat-dist*)
      (vspan *max-rat-dist*)
      (draw-on-top-group (add-group (current-screen) "tmp" :background t)))

  (add-group (current-screen) "aux" :background t)

  (labels ((clear-drawings ()
             (stumpwm:run-commands "gselect aux")
             (stumpwm:run-commands "gselect tmp"))
           (draw-reachable-rectangle ()
             (clear-drawings)
             (xlib:display-finish-output *display*)
             (multiple-value-bind (x y)
                 (xlib:global-pointer-position *display*)
               (draw-rectangle-onto-current-as-bg
                (- x hspan) (- y vspan) (* 2 hspan) (* 2 vspan))))
           (switch-to-draw-group ()
             (when (not (equal draw-on-top-group (current-group)))
               (screenshot-as-background)
               (stumpwm:run-commands "gselect tmp"))))

    (defcommand rat-bisect-left () ()
      "Move rat left half current hspan."
      (switch-to-draw-group)
      (ratrelwarp (- (floor hspan 2)) 0)
      (setf hspan (floor hspan 2))
      (when (eq 0 hspan)
        (setf hspan *max-rat-dist*))
      (draw-reachable-rectangle))

    (defcommand rat-bisect-right () ()
      "Move rat right half current hspan."
      (switch-to-draw-group)
      (ratrelwarp (floor hspan 2) 0)
      (setf hspan (floor hspan 2))
      (when (eq 0 hspan)
        (setf hspan *max-rat-dist*))
      (draw-reachable-rectangle))

    (defcommand rat-bisect-up () ()
      "Move rat up half current vspan."
      (switch-to-draw-group)
      (ratrelwarp 0 (- (floor vspan 2)))
      (setf vspan (floor vspan 2))
      (when (eq 0 vspan)
        (setf vspan *max-rat-dist*))
      (draw-reachable-rectangle))

    (defcommand rat-bisect-down () ()
      "Move rat down half current vspan."
      (switch-to-draw-group)
      (ratrelwarp 0 (floor vspan 2))
      (setf vspan (floor vspan 2))
      (when (eq 0 vspan)
        (setf vspan *max-rat-dist*))
      (draw-reachable-rectangle))

    (defcommand rat-click-left () ()
      "Get out of the view-draw state and click at current position."
      (if (equal draw-on-top-group (current-group))
          (stumpwm:run-commands "gselect Default"))
      (stumpwm:run-commands "ratclick"))))


;; key bindings
(define-key *top-map* (kbd "s-Left") "rat-bisect-left")
(define-key *top-map* (kbd "s-Up") "rat-bisect-up")
(define-key *top-map* (kbd "s-Down") "rat-bisect-down")
(define-key *top-map* (kbd "s-Right") "rat-bisect-right")
(define-key *top-map* (kbd "s-b") "blured-screenshot-as-background-and-clear-frames")
(define-key *top-map* (kbd "s-c") "rat-click-left")


;;;; rat-bisect-move.lisp ends here
