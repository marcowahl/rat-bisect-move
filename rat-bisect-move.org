#+title: Tasks for rat-bisect-move

* TODO Bind keys like iresize
* TODO Display only the current rectangle
* TODO Realize mode-like behavior

Usecase:

- Action :: Enter rat-bisect-move-mode.
- Assert :: Have all the graphical indicators.
- Action :: Move the rat.
- Action :: Leave the mode.
- Assert :: The rat kept its position.
- Assert :: The windows are restored as they were when the mode has
     been entered.
